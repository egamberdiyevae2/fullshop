import "./App.css";
import { createContext, useState } from "react";
import Routerr from "./Routes/Routerr";
export const BasketContext = createContext();

function App() {
  const [basket, setBasket] = useState([]);
  const addBasket = (item) => {
    if (basket.indexOf(item) !== -1) return;
    setBasket([...basket, item]);
  };
  const deleteBasket = (id) => setBasket(basket.filter((_) => _.id !== id));
  return (
    <BasketContext.Provider
      value={{ basket, setBasket, addBasket, deleteBasket }}
    >
      <div className="App">
        <Routerr />
      </div>
    </BasketContext.Provider>
  );
}

export default App;
