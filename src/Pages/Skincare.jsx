import React, { useEffect, useState } from "react";
import { useAxios } from "../hooks/useAxios";
import Cards from "../components/CategoryCards";
import Loader from "../components/Loader";

const Skincare = () => {
  const { res, loader } = useAxios("/products");
  const [products, setProducts] = useState();
  const laptopFilter = (arr) => {
    let laptops = arr.filter((element) => element.category === "skincare");

    setProducts(laptops);
  };

  useEffect(() => {
    laptopFilter(res);
  }, [res]);

  return (
    <div>
      {loader ? (
        <Loader />
      ) : (
        products?.map((data, id) => <Cards data={data} key={id} />)
      )}
    </div>
  );
};

export default Skincare;
