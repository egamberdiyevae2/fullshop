import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import useAxios from "../hooks/useAxios";
import { BasketContext } from "../App";

const Customer = () => {
  const [custlist, custupdate] = useState([]);
  const [haveedit, editchange] = useState(false);
  const [haveview, viewchange] = useState(false);
  const [haveadd, addchange] = useState(false);
  const [haveremove, removechange] = useState(false);
  const [posts, setPosts] = useState("");
  const { basket, setBasket } = useContext(BasketContext);

  const navigate = useNavigate();

  useEffect(() => {
    GetUserAccess();
    loadcustomer();
  }, []);

  const loadcustomer = () => {
    fetch("http://localhost:8000/customer")
      .then((res) => {
        if (!res.ok) {
          return false;
        }
        return res.json();
      })
      .then((res) => {
        custupdate(res);
      });
  };

  const GetUserAccess = () => {
    const userrole =
      sessionStorage.getItem("userrole") != null
        ? sessionStorage.getItem("userrole").toString()
        : "";
    fetch(
      "http://localhost:8000/roleaccess?role=" + userrole + "&menu=customer"
    )
      .then((res) => {
        if (!res.ok) {
          navigate("/");
          toast.danger("You are not authorized to access");
          return false;
        }
        return res.json();
      })
      .then((res) => {
        if (res.length > 0) {
          viewchange(true);
          let userobj = res[0];
          editchange(userobj.haveedit);
          addchange(userobj.haveadd);
          removechange(userobj.havedelete);
        } else {
          navigate("/");
          toast.warning("You are not authorized to access");
        }
      });
  };

  const handleadd = () => {
    if (haveadd) {
      toast.success("added");
    }
  };
  const handleedit = (id) => {
    // if (haveedit) {

    fetch(`http://localhost:8005/products/${id}`, {
      method: "PUT" /* or PATCH */,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "hello",
        // price: `${}`,
      }),
    })
      .then((res) => res.json())
      .then();
    toast.success("edited");
  };
  const { res } = useAxios("/products");
  // console.log(res);

  const deleteItem = (id) => {
    fetch(`http://localhost:8005/products/${id}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((id) => setBasket(basket.filter((_) => _.id !== id)));
    toast.success("removed");
  };
  return (
    <div className="container">
      <div className="card">
        <div className="card-header">
          <h3>Customer Listing</h3>
        </div>
        <div className="card-body">
          <button onClick={handleadd} className="btn btn-success">
            Add (+)
          </button>
          <br></br>

          <table className="table table-bordered">
            <thead className="bg-dark text-white">
              <tr>
                <th>id</th>
                <th>images</th>
                <th>Name</th>
                <th>Price</th>
                <th>Categories</th>
                <th>Brand</th>
                <th>Discount</th>
              </tr>
            </thead>

            <tbody>
              {res?.map((data) => (
                <tr key={data.title}>
                  <td>{data.id}</td>
                  <td>
                    <img
                      src={data.thumbnail}
                      width={"100px"}
                      height={"50px"}
                      alt=""
                    />
                  </td>
                  <td>{data.title}</td>
                  <td>{data.price}$</td>
                  <td>{data.category}</td>
                  <td>{data.brand}</td>
                  <td>{data.discountPercentage}% OFF</td>

                  <td>
                    <button
                      onClick={() => handleedit(data.id)}
                      className="btn btn-primary"
                    >
                      Edit
                    </button>{" "}
                    |
                    <button
                      onClick={() => deleteItem(data.id)}
                      className="btn btn-danger"
                    >
                      Remove
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Customer;
