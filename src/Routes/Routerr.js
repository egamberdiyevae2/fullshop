import React from "react";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import RootLayout from "../layout/RootLayout";
import Products from "../components/Products";
import ProductLayout from "../layout/ProductLayout";
import ProductDetail from "../components/ProductDetail";
import Basket from "../components/Basket";
import Laptops from "../Pages/Laptops";
import Smartphones from "../Pages/Smartphones";
import Fragrances from "../Pages/Fragrances";
import Skincare from "../Pages/Skincare";
import HomeDecoration from "../Pages/HomeDecoration";
import Groceries from "../Pages/Groceries";
import Login from "../components/Login";
import Registration from "../components/Registration";
import Dashboard from "../Pages/Dashboard";
import { ToastContainer } from "react-toastify";
import Appheader from "../components/Appheader";
import Customer from "../Pages/Customer";
import PrivateComponent from "../components/PrivateComponent";

const Router = () => {
  return (
    <div className="Routes">
      <ToastContainer theme="colored" position="top-center"></ToastContainer>

      <BrowserRouter>
        <Appheader></Appheader>
        <Routes>
          <Route path="/" element={<RootLayout />}>
            <Route index element={<Products />} />
            <Route path={"products"} element={<ProductLayout />}>
              <Route index element={<Products />} />
              <Route path={":id"} element={<ProductDetail />} />
            </Route>
            <Route path="carts" element={<Basket />} />
            <Route path="laptops" element={<Laptops />} />
            <Route path="smartphones" element={<Smartphones />} />
            <Route path="fragrances" element={<Fragrances />} />
            <Route path="skincare" element={<Skincare />} />
            <Route path="fragrances" element={<Fragrances />} />
            <Route path="homedecoration" element={<HomeDecoration />} />
            <Route path="groceries" element={<Groceries />} />
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Registration />} />

            <Route
              path="/customer"
              element={
                // <PrivateComponent>
                <Customer />
                // </PrivateComponent>
              }
            ></Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default Router;
