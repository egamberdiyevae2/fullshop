import React, { createContext, useState } from "react";
import Routerr from "../Routes/Routerr";
export const BasketContext = createContext();

const Context = () => {
  const [basket, setBasket] = useState([]);
  console.log(basket);

  const addBasket = (item) => {
    if (basket.indexOf(item) !== -1) return;
    setBasket([...basket, item]);
  };

  const deleteBasket = (id) => setBasket(basket.filter((_) => _.id !== id));
  return (
    <BasketContext.Provider
      value={{ basket, setBasket, addBasket, deleteBasket }}
    >
      <div className="App"></div>
    </BasketContext.Provider>
  );
};

export default Context;
