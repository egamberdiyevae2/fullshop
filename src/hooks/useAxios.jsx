import axios from "axios";
import { useEffect, useState } from "react";

axios.defaults.baseURL = "http://localhost:8005";

export function useAxios(url) {
  const [err, setErr] = useState("");
  const [res, setRes] = useState([]);
  const [loader, setLoader] = useState(true);
  useEffect(() => {
    return async () => {
      try {
        const res = await axios.get(url);
        setRes(res.data);
      } catch (err) {
        setErr(err);
      } finally {
        setLoader(false);
      }
    };
  }, []);
  return { res, err, loader };
}

export default useAxios;
