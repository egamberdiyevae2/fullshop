import React from "react";
import { Outlet } from "react-router-dom";

const ProductLayout = () => {
  return (
    <div>
      <h1 className="text-center"></h1>
      <div className="container">
        <Outlet />
      </div>
    </div>
  );
};

export default ProductLayout;
