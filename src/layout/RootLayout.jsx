import React, { useContext } from "react";
import { NavLink, Outlet } from "react-router-dom";
// import Basket from "../components/Basket";
// import { BasketContext } from "../Context/Context";
import { BasketContext } from "../App";
const RootLayout = () => {
  const { basket } = useContext(BasketContext);
  return (
    <div className="container ">
      <nav className="navbar navbar-expand-lg bg-success bg-gradient sticky-top ">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <NavLink to={"/products"} className="nav-link" href="#">
                Products
              </NavLink>
              <NavLink to={"/laptops"} className="nav-link" href="#">
                Laptops
              </NavLink>
              <NavLink to={"/smartphones"} className="nav-link" href="#">
                Smartphone
              </NavLink>
              <NavLink to={"/fragrances"} className="nav-link" href="#">
                Fragrances
              </NavLink>
              <NavLink to={"/skincare"} className="nav-link" href="#">
                Skincare
              </NavLink>
              <NavLink to={"/homedecoration"} className="nav-link" href="#">
                HomeDecoration
              </NavLink>
              <NavLink to={"/groceries"} className="nav-link" href="#">
                Groceries
              </NavLink>
            </div>
          </div>
          <div className="navbar-nav">
            <NavLink to={"/carts"}>
              <button className="btn btn-warning">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-cart3"
                  viewBox="0 0 16 16"
                >
                  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                </svg>
                <div className="badge bg-danger ms-3">{basket.length}</div>
              </button>
            </NavLink>
          </div>
        </div>
      </nav>
      <Outlet />
      {/* <Basket/> */}
    </div>
  );
};

export default RootLayout;
