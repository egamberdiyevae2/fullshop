import React, { useContext } from "react";
import { BasketContext } from "../App";

const Cards = ({ data }) => {
  const { basket, setBasket } = useContext(BasketContext);
  const addBasket = (item) => {
    if (basket.indexOf(item) !== -1) return;
    setBasket([...basket, item]);
  };
  return (
    <div>
      <div className="">
        <div className="row mb-5 d-flex align-items-center">
          <div className="col-md-4">
            <img src={data?.thumbnail} alt="" className="w-100" />
          </div>
          <div className="col-md-8 text-start">
            <h1> {data?.title}</h1>
            <p> {data?.description}</p>
            <p>
              <span className="text-danger"> Brand:</span> {data?.brand}
            </p>
            <p>
              {" "}
              <span className="text-danger"> Rating:</span> {data?.rating}
            </p>
            <p>
              <span className="text-danger"> Category:</span> {data?.category}
            </p>
            <h3>
              {" "}
              {data?.price}$
              <span className="text-danger px-5">
                {" "}
                {data?.discountPercentage}% OFF{" "}
              </span>
            </h3>

            <button
              className="btn btn-outline-warning btn-lg"
              onClick={() => addBasket(data)}
            >
              Add to card
            </button>

            <button className="btn btn-warning btn-lg ms-5">Buy now</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cards;
