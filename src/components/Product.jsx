import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { BasketContext } from "../App";

const Product = ({
  thumbnail,
  title,
  price,
  id,
  brand,
  description,
  rating,
  category,
  discountPercentage,
}) => {
  const { addBasket } = useContext(BasketContext);

  function addNewProduct(e) {
    e.preventDefault();
    addBasket({
      thumbnail,
      title,
      id,
      price,
      brand,
      description,
      rating,
      category,
      discountPercentage,
    });
  }

  return (
    <div className="col-md-3 mb-5">
      <div className="card">
        <img src={thumbnail} className="card-img-top product-img" alt={title} />
        <div className="card-body">
          <NavLink
            className="text-decoration-underline text-primary"
            to={"products/" + id}
          >
            <h5 className="card-title ">
              {title?.split(" ").slice(0, 2).join("")}
            </h5>
          </NavLink>
          <p className="card-text">{price}$</p>
          <a href="#h" className="btn btn-success" onClick={addNewProduct}>
            Add Cart
          </a>
        </div>
      </div>
    </div>
  );
};

export default Product;
