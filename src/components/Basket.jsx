// import Product from "./Product";
// import { BasketContext } from "../Context/Context";
import React, { useContext } from "react";
import { BasketContext } from "../App";

const Basket = () => {
  const { basket, setBasket } = useContext(BasketContext);
  const RemoveBasket = (id) => setBasket(basket.filter((_) => _.id !== id));

  return (
    <>
      <div>
        {basket.map((item) => {
          return (
            <>
              <div className="mb-3 " key={item.title}>
                <div className="border border-black d-flex p-4 justify-content-between  align-items-center">
                  <img
                    src={item.thumbnail}
                    width="200px"
                    height="150px"
                    alt="img"
                  />
                  <div className="d-flex flex-column ">
                    <h1>{item.title}</h1>
                    <p>{item.price}$</p>
                    <p>Brand:{item.brand}</p>
                  </div>
                  <button
                    className="btn btn-danger"
                    onClick={() => RemoveBasket(item.id)}
                  >
                    Remove
                  </button>
                </div>
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};

export default Basket;
