import React from "react";

const PrivateComponent = ({ children }) => {
  return { children };
};

export default PrivateComponent;
